<?php

abstract class Model {

	public function __construct() {

		// Silence is golden
	}

	abstract public function getData();
}