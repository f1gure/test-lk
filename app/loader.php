<?php

require_once ('config.php');
require_once ('route.php');
require_once ('db.php');
require_once ('helpers.php');
require_once ('model.php');
require_once ('controller.php');
require_once ('view.php');

Route::start();