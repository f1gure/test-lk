<?php

class Route {

	public static function start() {

		$controller = 'Main';
		$action = 'Index';

		$routes = explode('/', $_SERVER['REQUEST_URI']);

		if (!empty($routes[1]) && substr($routes[1], 0, 1) != '?') {
			$controller = ucfirst($routes[1]);
		}

		if (!empty($routes[2]) && substr($routes[2], 0, 1) != '?') {
			$action = ucfirst($routes[2]);
		}

		$controllerName = $controller . 'Controller';
		$action = 'action' . $action;

		$modelFile = "app/models/" . strtolower($controller) . '.php';

		if (file_exists($modelFile)) {

			include_once ($modelFile);
		}

		$controllerFile = "app/controllers/" . strtolower($controller) . '.php';

		if (file_exists($controllerFile)) {

			include_once ($controllerFile);

		} else {

			return self::error404();
		}

		// создаем контроллер
		$controller = new $controllerName;

		if (method_exists($controller, $action)) {

			$controller->$action();

		} else {

			return self::error404();
		}

		return true;
	}

	public static function error404() {

		header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");

		//Helpers::redirect('/404.php');

		return false;
	}
}