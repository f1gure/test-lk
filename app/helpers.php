<?php

class Helpers {

	static public function redirect($page) {

		header("Location: $page");

		return true;
	}

	static public function clearTags($string, $boolClearAll = true) {

		if (strlen($string) > 0) {

			if (!$boolClearAll) {

				return strip_tags($string, '<br /><p><a><b><strong><i><hr /><span><h2><h3><h4><h5><h6>');

			} else {

				return strip_tags($string);
			}
		}

		return false;
	}

	static public function formatDate($stringDate) {

		return date("d.m.Y H:i:s", strtotime($stringDate));
	}
}