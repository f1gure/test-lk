<?php

class View {

	public function __construct() {

		// Silence is golden
	}

	public function generate($template = 'main', $data = array()) {

		$template = 'app/views/' . $template . '.php';

		if (file_exists($template)) {

			include_once('app/views/header.php');
			include_once($template);
			include_once('app/views/footer.php');

		} else {

			Route::error404();
		}
	}
}