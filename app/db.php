<?php

final class DB {

	private static $_instance;

	public static function getInstance() {

		if (!(self::$_instance instanceof self)) {

			self::$_instance = new self();
		}

		return self::$_instance;
	}

	private function __construct() {
		// Silence is golden
	}

	public function getConnection() {

		$connect = null;

		try {

			$connect = new \PDO('mysql:host=' . APP_DB_HOST . ';dbname=' . APP_DB_NAME, APP_DB_USER, APP_DB_PASSWORD);

			$connect->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

			return $connect;

		} catch(PDOException $e) {

			throw $e;

		} catch(Exception $e) {

			throw $e;

		}

	}

	public function __destruct() {
		// Silence is golden
	}

	private function __clone() {
		// Silence is golden
	}

	private function __sleep() {
		// Silence is golden
	}

	private function __wakeup() {
		// Silence is golden
	}
}