<?php

class AdminController extends Controller {

	public $model;

	protected $_arUser;

	private function getUserData() {

		if (!isset($this->_arUser)) {

			$this->model = new AdminModel();

			$arResult = $this->model->getData();

			if ($arResult["user"]) {

				$this->_arUser = $arResult["user"];

			} else {

				$this->_arUser = array();
			}


		}

		return $this->_arUser;
	}

	private function isAuth() {

		$arResult = $this->getUserData();

		if (!empty($arResult)) {

			return true;

		} else {

			return false;
		}

	}

	public function actionIndex() {

		$arResult = array();

		$arResult["page_title"] = 'Админ панель';

		$arResult['user'] = $this->getUserData();

		if ($this->isAuth()) {

			$arFilter = array();

			if (isset($_GET["remove"]) && $removeID = (int) filter_var($_GET["remove"])) {

				$this->model->removePage($removeID);
			}

			if (isset($_GET["search"]) && $search = filter_var(Helpers::clearTags($_GET["search"]))) {

				$arFilter['name'] = array(
					'type' => 'like',
					'value' => '%' . $search . '%'
				);

				$arResult["search"] = $search;

				$arResult["page_title"] = 'Созданные страницы по запросу "' . $search . '"';
			}

			$arSelect = array('id', 'name', 'date_change');

			$arResult['pages'] = $this->model->getPagesList($arSelect, $arFilter);

			$this->view->generate('admin/index', $arResult);

		} else {

			Helpers::redirect('/admin/auth/');

			return false;
		}

		return true;
	}

	public function actionLogout() {

		unset($_SESSION["userID"]);

		Helpers::redirect('/admin/auth/');

		return false;
	}

	public function actionAuth() {

		$arResult = array();

		$arResult["page_title"] = 'Авторизация';

		$this->getUserData();

		if ($this->isAuth()) {

			Helpers::redirect('/admin/');

			return false;

		} else {

			if ($_SERVER["REQUEST_METHOD"] == 'POST' && $_POST["submit"]) {

				$login = filter_var($_POST["login"]) ?: '';
				$password =  filter_var($_POST["password"]) ?: '';

				if ($login && $password) {

					if ($userID = $this->model->authUser($login, $password)) {

						$_SESSION["userID"] = $userID;
						Helpers::redirect('/admin/');

						return false;

					} else {

						$arResult["error"] = 'Не правильный логин или пароль';
					}

				} else {

					$arResult["error"] = 'Заполните все поля';
				}
			}

			$this->view->generate('admin/auth', $arResult);

			return true;
		}
	}

	private function preparePageParams($arParams) {

		$arReturn = array();

		$arReturn['name'] = filter_var(Helpers::clearTags($arParams['name'])) ?: '';
		$arReturn['content'] = filter_var(Helpers::clearTags($arParams['content'], false)) ?: '';
		$arReturn['additional'] = filter_var(Helpers::clearTags($arParams['additional'], false)) ?: '';
		$arReturn['seo_title'] = filter_var(Helpers::clearTags($arParams['seo_title'])) ?: '';
		$arReturn['seo_description'] = filter_var(Helpers::clearTags($arParams['seo_description'])) ?: '';

		return $arReturn;
	}

	public function actionPage() {

		$pageID = filter_var($_GET["id"]) ?: 0;

		$arResult['user'] = $this->getUserData();

		if ($this->isAuth()) {

			if ($pageID) {

				if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"]) {

					$arParams = $this->preparePageParams($_POST['page']);

					if ($arParams["name"] && $arParams["content"]) {

						$arParams["date_change"] = date("Y-m-d H:i:s");

						$arReturn = $this->model->updatePage($pageID, $arParams);

						if ($arReturn === true) {

							$arResult["success"] = 'Страница обновлена';

						} else {

							$arResult["error"] = $arReturn;
						}

					} else {

						$arResult["error"] = "Заполните обязательные поля";
					}
				}

				$arResult['page'] = $this->model->getPage($pageID);

				$arResult["page_title"] = 'Редактирование страницы "' . $arResult['page']['name'] . '"';

				$this->view->generate('admin/pageEdit', $arResult);

			} else {

				$arResult["page_title"] = 'Добавление новой страницы';

				if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"]) {

					$arParams = $this->preparePageParams($_POST['page']);

					if ($arParams["name"] && $arParams["content"]) {

						$arParams["date_creation"] = $arParams["date_change"] = date("Y-m-d H:i:s");

						$arReturn = $this->model->addPage($arParams);

						if ((int) $arReturn > 0) {

							Helpers::redirect('/admin/page/?id=' . $arReturn);
							
						} else {

							$arResult["error"] = $arReturn;
						}

					} else {

						$arResult["error"] = "Заполните обязательные поля";
					}
				}

				$this->view->generate('admin/pageAdd', $arResult);
			}

			return true;

		} else {

			Helpers::redirect('/admin/auth/');

			return false;
		}
	}
}