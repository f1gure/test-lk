<div class="container admin-add"><?
	require_once ('app/views/admin/header.php');
	?>
	<div class="row clear-margin">
		<h2>Добавление страницы</h2>
		<form class="form-horizontal" action="" method="post">
			<div class="form-group">
				<label class="control-label col-sm-2" for="name">Название:*</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="page[name]" id="name" placeholder="Название страницы" required value="<?=(htmlspecialchars($_POST['page']['name']) ?: '')?>" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="content">Контент:*</label>
				<div class="col-sm-10">
					<textarea class="form-control" name="page[content]" id="content" placeholder="Основной контент" required ><?=(htmlspecialchars($_POST['page']['content']) ?: '')?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="additional">Дополнительно:</label>
				<div class="col-sm-10">
					<textarea class="form-control" name="page[additional]" id="additional" placeholder="Дополнительный контент"><?=(htmlspecialchars($_POST['page']['additional']) ?: '')?></textarea>
				</div>
			</div>
			<div class="form-group">
				<h4 class="col-sm-2">SEO:</h4>
				<div class="col-sm-10"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="seo_title">Meta title:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="page[seo_title]" id="seo_title" value="<?=(htmlspecialchars($_POST['page']['seo_title']) ?: '')?>" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="seo_description">Meta description:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="page[seo_description]" id="seo_description" value="<?=(htmlspecialchars($_POST['page']['seo_description']) ?: '')?>" />
				</div>
			</div><?
			if ($data["ERROR"]):?>
				<div class="form-group">
					<div class="alert alert-danger">
						<strong>Ошибка!</strong> <?=$data["ERROR"]?>
					</div>
				</div><?
			endif;?>
			<div class="form-group align-right">
				<button name="submit" type="submit" class="btn btn-primary btn-margin" value="Y">Добавить</button>
				<a rel="nofollow" class="btn btn-link" href="/admin/" title="Отменить">Отменить</a>
			</div>
		</form>
	</div>
</div>