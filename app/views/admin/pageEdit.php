<div class="container admin-add"><?
	require_once ('app/views/admin/header.php');
	?>
	<div class="row clear-margin">
		<h2>Изменение страницы "<?=$data['page']['name']?>" [<?=$data['page']['id']?>]</h2>
        <p class="align-right">Дата создания страницы: <?=Helpers::formatDate($data['page']['date_creation'])?><br />Дата изменения страницы: <?=Helpers::formatDate($data['page']['date_change'])?></p>
		<form class="form-horizontal" action="" method="post">
			<div class="form-group">
				<label class="control-label col-sm-2" for="name">Название:*</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="page[name]" id="name" placeholder="Название страницы" required value="<?=$data['page']['name']?>" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="content">Контент:*</label>
				<div class="col-sm-10">
					<textarea class="form-control" name="page[content]" id="content" placeholder="Основной контент" required ><?=$data['page']['content']?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="additional">Дополнительно:</label>
				<div class="col-sm-10">
					<textarea class="form-control" name="page[additional]" id="additional" placeholder="Дополнительный контент"><?=$data['page']['additional']?></textarea>
				</div>
			</div>
			<div class="form-group">
				<h4 class="col-sm-2">SEO:</h4>
				<div class="col-sm-10"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="seo_title">Meta title:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="page[seo_title]" id="seo_title" value="<?=$data['page']['seo_title']?>" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="seo_description">Meta description:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="page[seo_description]" id="seo_description" value="<?=$data['page']['seo_description']?>" />
				</div>
			</div><?
            if ($data['success']):?>
                <div class="form-group">
                    <div class="alert alert-success">
                        <strong><?=$data['success']?></strong>
                    </div>
                </div>
            <?elseif ($data['error']):?>
				<div class="form-group">
					<div class="alert alert-danger">
						<strong>Ошибка!</strong> <?=$data['error']?>
					</div>
				</div><?
			endif;?>
			<div class="form-group align-right">
                <input type="hidden" name="id" value="<?=$data['page']['id']?>" />
				<button name="submit" type="submit" class="btn btn-primary btn-margin" value="Y">Сохранить</button>
				<a rel="nofollow" class="btn btn-link" href="/admin/" title="Отменить">Отменить</a>
			</div>
		</form>
	</div>
</div>