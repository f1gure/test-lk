<div class="container auth-form">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="row">
				<h1>Авторизация</h1>
				<form class="form-horizontal" action="" method="post">
					<div class="form-group">
						<label class="control-label col-sm-2" for="login">Логин:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="login" id="login" placeholder="Введите логин" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd">Пароль:</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" name="password" id="pwd" placeholder="Введите пароль" required />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" name="submit" class="btn btn-primary btn-block" value="Y">Войти</button>
						</div>
                    </div><?
                    if ($data["error"]):?>
                        <div class="alert alert-danger">
                            <strong>Ошибка!</strong> <?=$data["error"]?>
                        </div><?
                    endif;
                    ?>
				</form>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
</div>