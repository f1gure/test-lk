<div class="container admin-list"><?
    require_once ('app/views/admin/header.php');
    ?>
	<div class="row">
        <div class="col-md-3">
            <a rel="nofollow" href="page/" class="btn btn-primary btn-block">Добавить страницу</a>
        </div>
		<div class="col-md-9">
			<form method="GET" action="" class="input-group page-search">
				<input type="text" name="search" class="form-control" placeholder="Найти по названию" value="<?=(htmlspecialchars($_GET['search']) ?: '')?>" />
				<div class="input-group-btn">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
					</div>
				</div>
			</form>
		</div>
	</div>
    <div class="row pages-list clear-margin"><?

        if ($data["search"]):?>
            <h2>Найденные страницы по запросу "<?=$data["search"]?>"</h2><?
        endif;

        if (!empty($data["pages"])):
        ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Название</th>
                        <th>Дата изменения</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody><?
                    foreach ($data["pages"] as $page):
                    ?>
                        <tr>
                            <td><?=$page["id"]?></td>
                            <td><?=$page["name"]?></td>
                            <td><?=Helpers::formatDate($page["date_change"])?></td>
                            <td style="width: 110px;">
                                <a href="page/?id=<?=$page["id"]?>" class="btn btn-default glyphicon glyphicon-pencil"></a>
                                <button data-href="?remove=<?=$page["id"]?>" class="btn btn-default remove-page glyphicon glyphicon-remove"></button>
                            </td>
                        </tr><?
                    endforeach;
                    ?>
                </tbody>
            </table>
        <?else:
        ?>
            <p>Не добавлено ни одной страницы сайта</p>
        <?endif;?>
    </div>
</div>