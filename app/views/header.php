<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?=($data["page_title"] ?: '')?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="/media/styles.css" />
	</head>
	<body>