<?php

class AdminModel extends Model {

	public function getData() {

		$userID = (int) filter_var($_SESSION["userID"], FILTER_VALIDATE_INT);

		$arReturn = array();

		if ($userID) {

			$db = DB::getInstance()->getConnection();

			$sth = $db->prepare('SELECT id, name FROM `users` WHERE id = ? LIMIT 1');

			$sth->execute(array($userID));

			$arReturn["user"] = $sth->fetchall(\PDO::FETCH_ASSOC)[0];
		}

		return $arReturn;
	}

	public function authUser($login, $password) {

		$hash = md5(md5($password) . APP_SALT);

		$db = DB::getInstance()->getConnection();

		$sth = $db->prepare('SELECT id FROM `users` WHERE login = ? AND password = ? LIMIT 1');

		$sth->execute(array($login, $hash));

		if ($arReturn = $sth->fetchall(\PDO::FETCH_ASSOC)[0]['id']) {

			return $arReturn;

		}

		return false;
	}

	public function addPage($arParams) {

		if (count($arParams) > 0) {

			$db = DB::getInstance()->getConnection();

			$columns = array_keys($arParams);

			$query = 'INSERT INTO `pages` (' . implode(',', $columns) . ') VALUES (' . str_repeat('?,', count($columns) - 1) . '?)';

			$sth = $db->prepare($query);

			$sth->execute(array_values($arParams));

			if ($id = $db->lastInsertId()) {

				return $id;

			} else {

				return 'Ошибка, проверьте поля';
			}
		}

		return 'Заполните все поля';
	}

	public function getPagesList($arSelect = array(), $arFilter = array()) {

		$db = DB::getInstance()->getConnection();

		if (!empty($arSelect)) {

			$select = implode(', ', $arSelect);

		} else {

			$select = '*';
		}

		$query = 'SELECT ' . $select . ' FROM `pages`';

		if (!empty($arFilter)) {

			foreach ($arFilter as $key => $val) {

				if (!$val['type']) {

					$val['type'] = '=';
				}

				$arFilterString[] = $key . ' ' . $val['type'] . " '" . $val['value'] . "'";
			}

			if (!empty($arFilterString)) {

				$query .= ' WHERE ' . implode(' AND ', $arFilterString);
			}
		}

		$sth = $db->query($query);

		$sth->execute();

		if ($arReturn = $sth->fetchall(\PDO::FETCH_ASSOC)) {

			return $arReturn;
		}

		return false;
	}

	public function getPage($id, $arSelect = array()) {

		if ((int) $id > 0) {

			$db = DB::getInstance()->getConnection();

			if (!empty($arSelect)) {

				$select = implode(', ', $arSelect);

			} else {

				$select = '*';
			}

			$sth = $db->prepare('SELECT ' . $select . ' FROM `pages` WHERE id = ?');

			$sth->execute(array((int) $id));

			if ($arReturn = $sth->fetchall(\PDO::FETCH_ASSOC)[0]) {

				return $arReturn;
			}
		}

		return false;
	}

	public function removePage($id) {

		if ((int) $id > 0) {

			$db = DB::getInstance()->getConnection();

			$sth = $db->prepare('DELETE FROM `pages` WHERE id = ?');

			if ($sth->execute(array((int) $id))) {

				return true;
			}

		}

		return false;
	}

	public function updatePage($id, $arParams) {

		if (count($arParams) > 0 && (int) $id > 0) {

			$db = DB::getInstance()->getConnection();

			$columns = array_keys($arParams);

			$query = 'UPDATE `pages` SET ' . implode(' = ? ,', $columns) . ' = ? WHERE id = ?';

			$sth = $db->prepare($query);

			$sth->execute(array_merge(array_values($arParams), array($id)));

			if ($sth->rowCount()) {

				return true;

			} else {

				return 'Ошибка, проверьте поля';
			}
		}

		return 'Заполните все поля';
	}
}