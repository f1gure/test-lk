
$(function () {

    $('.remove-page').on('click', function (e) {

        var href = $(this).data('href');

        e.preventDefault();

        if (confirm('Удалить страницу?')) {

            if (href) {

                location.href = href;
            }
        }

    });

});