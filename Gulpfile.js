var gulp = require('gulp'),
	stylus = require('gulp-stylus'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
    autoPrefixer = require('gulp-autoprefixer'),
	browserSync = require('browser-sync'),
	bootstrap = require('bootstrap-styl');

gulp.task('stylus', function () {

	return gulp.src('./media/stylus/main.styl')
		.pipe(stylus({'include css': true, use: bootstrap(), compress: true}))
		.pipe(autoPrefixer({browsers: ['> 2% in RU', 'ie 9', 'last 10 versions'], cascade: false}))
        .pipe(rename('styles.css'))
		.pipe(gulp.dest('./media/'))
		.pipe(browserSync.stream());
});

gulp.task('watch', function(cb) {
	
	browserSync.init({
		notify: false,
		https: false,
		open: false,
		proxy: 'lk.localhost'
	}, cb);

	process.on('exit', function() {
		browserSync.exit();
	});

	gulp.watch(['/media/stylus/**', '/media/css/**'], ['stylus']);
	
});

gulp.task('default', ['stylus', 'watch']);
gulp.task('build', ['stylus']);